#!/bin/bash

#Change to this directory
cd `dirname "$0"`

#Run python script
python import-external-python-modules.py
exit_code=$?
if [[ $exit_code == 1 ]]; then
    echo "One or more modules not found"
    exit 1
fi
