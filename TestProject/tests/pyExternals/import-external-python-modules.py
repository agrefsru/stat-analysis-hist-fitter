import sys

imports = ["pyhf", "pandas", "cabinetry"]
modules = []
failed_imports = 0
for x in imports:
    try:
        modules.append(__import__(x))
        #print ("Successfully imported ", x, '.')
    except ImportError:
        print ("Error importing ", x, '.')
        failed_imports+=1
if failed_imports > 0:
    sys.exit(1)
