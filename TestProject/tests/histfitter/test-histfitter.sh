#!/bin/bash

#Set up histfitter environment
mkdir histfitter_test
cd histfitter_test
source setup_histfitter.sh -t -e
exit_code=$?

#Test if histfitter setup script is available
if [[ $exit_code == 1 ]]; then
    echo "setup_histfitter.sh is not properly installed."
    exit 1

#If it is, continue the test
else
    #Run pytests
    pytest --disable-warnings
    exit_code=$?
    cd ..

    #Cleanup
    rm histfitter_test -rf

    #See if any tests failed
    if [[ $exit_code == 1 ]]; then
        echo "One or more tests did not pass."
        exit 1
    fi
fi


