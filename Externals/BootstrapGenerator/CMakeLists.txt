# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Package building BootstrapGenerator for StatAnalysis releases
#

# The name of the package:
atlas_subdir( BootstrapGenerator )

set( STATANA_BOOTSTRAPGENERATOR_REPOSITORY "${CERN_GITLAB_PREFIX}/atlas-physics/sm/StandardModelTools_BootstrapGenerator/BootstrapGenerator.git" CACHE STRING "Repository of BootstrapGenerator" )


set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/BootstrapGeneratorBuild )

if(STATANA_VERBOSE)
    set(_logging OFF)
else()
    set(_logging ON)
endif()

# Build lwtnn for the build area:
ExternalProject_Add( BootstrapGenerator
        PREFIX ${CMAKE_BINARY_DIR}
        INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
        GIT_REPOSITORY ${STATANA_BOOTSTRAPGENERATOR_REPOSITORY}
        GIT_TAG ${STATANA_BOOTSTRAPGENERATOR_VERSION}
        BUILD_ALWAYS ${TRACK_CHANGES}
        CMAKE_CACHE_ARGS
        -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
        -DCMAKE_PREFIX_PATH:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}  # ensures will discover the release's version of ROOT
        LOG_DOWNLOAD ${_logging} LOG_CONFIGURE ${_logging} LOG_BUILD ${_logging} LOG_INSTALL ${_logging}
        LOG_OUTPUT_ON_FAILURE 1
        UPDATE_COMMAND "" # needed for next line to work
        UPDATE_DISCONNECTED TRUE) # skips reconfigure+build if just rerunning.
ExternalProject_Add_Step( BootstrapGenerator buildinstall
        COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir} <INSTALL_DIR>
        COMMENT "Installing BootstrapGenerator into the build area"
        DEPENDEES install
        )
add_dependencies( Package_BootstrapGenerator BootstrapGenerator )

if( ATLAS_BUILD_ROOT )
    add_dependencies ( BootstrapGenerator ROOT )
endif()


# Install BootstrapGenerator:
install( DIRECTORY ${_buildDir}/
        DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

