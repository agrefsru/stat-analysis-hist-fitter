
foreach(foo IN ITEMS xRooFit TRExFitter RooFitExtensions RooUnfold workspaceCombiner xmlAnaWSBuilder quickFit BootstrapGenerator Significance HistFitter CommonStatTools)
    string(TOUPPER STATANA_${foo}_DATE VAR)
    message("Doing ${VAR} in ${BUILD_DIR}/src/${foo}")
    execute_process(
            COMMAND git log -1 --format=%cd --date=iso
            WORKING_DIRECTORY ${BUILD_DIR}/src/${foo}
            OUTPUT_VARIABLE ${VAR}
            OUTPUT_STRIP_TRAILING_WHITESPACE
    )
endforeach()

configure_file(${INPUT_FILE} ${OUTPUT_FILE})
